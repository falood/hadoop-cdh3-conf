export JAVA_HOME=/home/java/jdk1.6.0_27/
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin

export ANT_HOME=/home/java/apache-ant-1.8.2/
export PATH=$PATH:$ANT_HOME/bin
